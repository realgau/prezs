# Édition adaptée, édition inclusive : enjeux et complémentarités'

g.chomel\@gmail.com

Histoire du livre numérique
===========================

Le livre numérique
------------------

-   1971, projet Gutenberg (gutenberg.org) numérise les oeuvres du
    domaine public.
-   1981, premier livre électronique à la vente (Random House's
    Electronic Dictionary).
-   1985, premiers livres en CD-Rom.
-   1999, publication du standard Open eBook (OEB).
-   2007, publication du standard EPUB.

Histoires croisées
------------------

le World Wide Web

-   1990, première page web.
-   1994, création du W3C.
-   1995, début de la guerre des navigateurs.

Histoires croisées
------------------

les bibliothèques spécialisées

-   1994, création du standard propriétaire DAISY (Digital Accessible
    Information SYstem) en Suéde.
-   1996, création du consortium DAISY.
-   1997, le format DAISY devient une norme ouverte basée sur le format
    XML.
-   1998, DAISY 2.
-   2005, DAISY 3.

2011 : alignement des formats
-----------------------------

EPUB 3 est basé sur les technologies HTML 5 et CSS3.

Il y a désormais un seul et même format pour le grand public et pour les
personnes empêchées de lire.

2017 : Publishing\@W3C
----------------------

L'IDPF cesse ses activités et intègre le nouveau groupe de travail
Publishing Working Group du W3C.

Le futur des formats web et édition sont pensés dans le même cadre.

L'accès à la lecture
====================

La famine du livre
------------------

On estime à moins de 8% d'ouvrages accessibles ou adaptés disponibles en
France.

-   Braille.
-   Gros caractéres.
-   Livre numérique.

Principe
--------

L'accessibilité dépend de la combinaison entre la qualité du fichier, le
logiciel de lecture, la technologie d'assistance utilisée et les
compétences de l'usager.

Points clef
-----------

-   Ordre de lecture logique.
-   Séparation du style et du contenu.
-   Possibilités de navigation dans le fichier.
-   Contenus alternatifs.
-   Enrichissements sémantiques.

Chaîne de production
--------------------

L'accessibilité impacte

-   la rédaction des contenus ;
-   la conception graphique du document ;
-   la construction du fichier de mise en page ;
-   la finalisation du fichier avec un éditeur dédié.

Concept clefs
-------------

Tout document doit être considéré comme la combinaison de trois éléments
:

<span style="color:darkgreen">**Structure / Contenu / Aspect**</span>

-   <span style="color:darkgreen">Contenu</span> : textes, espaces et illustrations
    contextuelles.

-   <span style="color:darkgreen">Aspect :</span> typographies, embellissements,
    disposition géométrique de la page et de son contenu.

-   <span style="color:darkgreen">Structure :</span> séquence de chapitres, sections,
    en têtes, pieds de pages, sous titre, paragraphes.

Éditer en 2018
==============

Besoins
-------

-   Produire différents formats.
-   Commercialiser sur différentes plateformes.
-   Assurer la pérénité des fichiers dans le temps.

Enjeux
------

-   Inclure le plus grand nombre.
-   Préparer les évolutions législatives.
-   Organisation et modularité des chaînes de production.

L'édition inclusive
-------------------

-   Répond à des problématiques plus grandes que la simple inclusion.
-   Mais ne pourra répondre à chaque besoin individuellement.

Édition adaptée
===============

Des structures différentes
--------------------------

-   Services généralistes.
-   Services intégrés aux structures médico-sociales.

Enjeux
------

-   Répondre aux besoins spécifiques.
-   Traitement de contenus complexes.
-   Reprendre plusieurs fois le même ouvrage dans des formats
    différents.
-   Spécificitées du braille embossé ou numérique.

Adapter
-------

-   Scanner ou extraire du contenu.
-   Corriger, réorganiser, recopier.
-   Réorganiser le contenu de façon linéaire.
-   Redéfinir l\'aspect.
-   Reformuler des élèments pédagogiques.

Besoins
-------

-   Se concentrer sur le coeur de métier.
-   Savoir transformer des fichiers structurés.
-   Mutualiser les ouvrages adaptés.

Un ouvrage adapté
-----------------

-   Est une nouvelle édition.
-   Doit identifier précisément son niveau d\'accessibilité.
-   Doit avoir un ISBN différent de sa source.

Écrire l'histoire
=================

Résorber la famine littéraire
-----------------------------

-   Personne ne dispose des moyens nécessaires.
-   Cohésion d\'action nécessaire.
-   Mobiliser et faire évoluer un écosystéme complet.

écosystéme
----------

Éditeurs

-   commerciaux
-   non commerciaux

Concepteurs des solutions de lecture

Concepteurs de matériel spécifique

Gouvernements et cadre législatif

Plateformes de diffusion

-   Médiathéques
-   Librairies

Usagers

L'accessibilité est plus qu'une simple checklist
------------------------------------------------

-   Compréhension des besoins spécifiques.
-   Acquisition d'une culture.

Évolutions professionelles
--------------------------

Les apports des publics empéchés de lire vont plus loin que leurs seules
nécessitées.

-   Sortir des silos propriétaires.
-   Anticiper le futur et les évolutions.
-   S'inscrire dans l'universalité.

Merci !
=======

g.chomel\@gmail.com
-------------------

