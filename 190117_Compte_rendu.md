

<head>
	<title>Nativement accessible et édition adaptée</title>
	<meta name="description" content="Édition adaptée et transition vers le nativement accessible">
	<meta name="author" content="Gautier Chomel">

</head>


# Nativement accessible et édition adaptée

*Atelier du 17 janvier 2019 dans le cadre des Rencontres Nationales du Livre Numérique Accessible à l'enssib, animé par Gautier Chomel.*

## Résumé

L'atelier a débuté par une mise en contexte de l'édition inclusive et de l'édition adaptée après 8 ans de mise en pratique de la loi exception handicap. Le contenu de cette contextualisation est disponible ci-dessous. 

Nous avons ensuite discuté et émis des hypothèses sur la place de l'édition adaptée dans les mutations en cours et des pistes pour améliorer de façon significative la production rapide d'adaptations de qualité pour les lecteurs nécessitant des formats spécifiques.

Ces discussions ont fait le constat que certains formats fournis par les éditeurs facilitent grandement le travail d'adaptation notamment en libérant le transcripteur des taches d'extraction et de correction de contenus. Cependant plusieurs professionnels sont en difficultés car il ne possèdent pas les connaissances ou les outils pour tirer parti de ces formats. Le temps nécessaire à l'appropriation de ces techniques est également un facteur de difficultés. La mutualisation de certaines compétences avancées serait un plus mais reste complexe à organiser.

Les efforts de mutualisation sont également ralentis et d'efficacité limité car les formats produits ne sont pas tous structurés suffisament pour pouvoir être modifiés efficacement. Une harmonisation des pratiques de production d'ouvrages adaptés serait souhaitable mais reste complexe à mettre en place du fait de la diversité et du nombre de structures.

L'identification exacte des ouvrages adaptés est aussi un frein au partage, il est important d'établir des fiches techniques de chaque ouvrage, répertoriants les modifications apportées à la forme ou à la structure. Ces informations seraient la base d'un système de métadonnées robuste qui permettrait aux lecteurs de trouver le fichier exact qui leur conviens, mais également aux professionnels de savoir quel ouvrage sera adaptable rapidement. 

Concernant les usages des manuels numériques, les lecteurs manquent d'informations, et sont souvent peu informés des possibilités du format epub3, des offre légales accessibles ou des possibilités offertes par les médiathèques spécialisées.

L'édition adaptée n'échappe pas aux mutations en cours dans le secteur. C'est une occasion de se recentrer sur le coeur de métier : répondre aux besoins spécifiques que ne peut couvrir l'éditeur. 

L'expertise des professionels est ammenée à se recentrer sur une connaissance des besoins spécifiques, une compréhension des compensations mises en place par les lecteurs, et une pratique de la transformation de fichiers. Des outils utiles aujourd'hui pour penser des ouvrages inclusifs et des chaines de publications interopérables. 

## Contextualisation

### Éditer en 2019

#### Besoins

* Produire différents formats.
* Commercialiser sur différentes plateformes.
* Assurer la pérennité des fichiers dans le temps.

#### Enjeux

* Inclure le plus grand nombre.
* Préparer les évolutions législatives.
* Modularité des chaînes de production.

#### L'édition inclusive

* Répond à des problématiques plus grandes que la simple inclusion.
* Mais ne pourra répondre à chaque besoin individuellement.

#### L'accessibilité est plus qu'une check-list

- Compréhension des besoins spécifiques.
- Acquisition d'une culture.

### L'édition adaptée en 2019

#### Des structures différentes

* Services généralistes (BNFA, EOLE, Médiathèques).
* Services intégrés aux structures médico-sociales.

#### Enjeux

* Répondre aux besoins spécifiques.
* Traiter des contenus complexes.
* Reprendre plusieurs fois le même ouvrage dans des formats différents.
* Connaître les spécificités du code braille.

#### Réalité quotidienne

- Scanner ou extraire du contenu.
- Corriger, réorganiser, recopier.
- Réorganiser le contenu de façon linéaire.
- Redéfinir l'aspect.
- Reformuler des éléments pédagogiques.
- Connaitre les spécificités du braille embossé ou numérique.

#### Évolutions depuis 2010

* Moins de scan mais toujours des erreurs d'ocr ou d'extraction du contenu des pdfs. 
* Manque de temps pour comprendre et apprendre à utiliser certains formats.
* Manque de moyens logiciels pour ouvrir des fichiers propriétaires. 
* Traitement rapide des fichiers pdfs, mais peu de travail d'adaptation, et une expérience de lecture complexe pour les usagers. 

#### Besoins

- Se concentrer sur le coeur de métier.
- Savoir transformer des fichiers structurés.
- Mutualiser les ouvrages adaptés.

### Pour résorber la famine littéraire, personne ne dispose des moyens nécessaires.

- Cohésion d'action nécessaire.
- Mobiliser et faire évoluer un écosystème complet.

#### écosystème

* Éditeurs 
 	* commerciaux
 	* non commerciaux
* Concepteurs 
 	* des solutions de lecture
 	* de matériel spécifique
* Gouvernements et cadre législatif
* Plateformes de diffusion
 	* Médiathèques 
 	* Librairies
* Lecteurs
